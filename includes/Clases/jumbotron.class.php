<?php 

// includes/classes/jumbotron.class.php

Class Jumbotron{

	public $titulo;
	public $texto;
	public $textoEnlace;
	public $urlEnlace;

	function __construct(){
		$this->titulo='';
		$this->texto='';
		$this->textoEnlace='';
		$this->urlEnlace='';
}
	function dibujame(){

		global $j;
		
		$resultado='<div class="jumbotron">';
  		$resultado.='<div class="container">';
   		$resultado.='<h1>'.$j->titulo.'</h1>';
   		$resultado.='<p>'.$j->texto.'</p>';
   		$resultado.='<p><a class="btn btn-primary btn-lg" role="button" href="'.$j->urlEnlace.'">'.$j->textoEnlace.'</a></p>';
  		$resultado.='</div>';
  		$resultado.='</div>';

  		return $resultado;
	}
}
 ?>
