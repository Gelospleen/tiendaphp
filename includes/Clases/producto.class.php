<?php 

//Fichero: includes/classes/producto.class.php

Class Producto{

	public $idProd;
	public $nombreProd;
	public $descripcionProd;
	public $precioProd;
	public $unidadesProd;
	public $fechaAlta;
	public $activado;
	public $idCat;
	public $imagenes;

	function __construct($fila){
		global $conexion; //Meto aqui la conexion a BBDD
		$this->idProd=$fila['idProd'];
		$this->nombreProd=$fila['nombreProd'];
		$this->descripcionProd=$fila['descripcionProd'];
		$this->precioProd=$fila['precioProd'];
		$this->unidadesProd=$fila['unidadesProd'];
		$this->fechaAlta=$fila['fechaAlta'];
		$this->activado=$fila['activado'];
		$this->idCat=$fila['idCat'];
		$this->imagenes=array();

		$sql="SELECT * FROM imagenes WHERE idProd=.$this->idProd";
		$consulta=mysqli_query($conexion, $sql);
		while($r=mysqli_fetch_array($consulta)){
			$this->imagenes[]=$r['archivoImg'];
		}
	}

	function dimeIva(){
		return $this->precioProd * 0.21;
	}

}


 ?>