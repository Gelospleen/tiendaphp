<?php 

// includes/classes/imagen.class.php

Class Imagen{
	public $archivo;
	public $ancho;

	public $redondeada;


	function __construct($archivo, $ancho="100%"){
		$this->archivo=$archivo;
		$this->ancho=$ancho;
		
		$this->redondeada=true;

	}


	function dibujaImagen(){
		if($this->redondeada==true){
			$c='img-rounded';
		}else{
			$c='';
		}

		$resultado='';
		$resultado='<img src="'.$this->archivo.'" width="'.$this->ancho.'"  class="'.$c.'">';
		return $resultado;


	}
}

 ?>