<?php 
	
Class Menu{
	//Propiedades
	public $enlaces;
	public $nombres;
	public $tipo;

	//Metodos
	function __construct($urlNombre, $urlEnlace){
		$this->enlaces=$urlEnlace;
		$this->nombres=$urlNombre;
		$this->tipo='nav-tabs';
	}

	function dibujaMenu(){
		global $p;
		$resultado='';
		$resultado.='<ul class="nav '.$this->tipo.'">';
			for ($i=0; $i < count($this->nombres) ; $i++) { 
				if($this->enlaces[$i]==$p){
					$c='active';
				}else{
					$c='';
				}
				$resultado.='<li class="'.$c.'">';
				$resultado.='<a href="index.php?p='.$this->enlaces[$i].'">';

				$resultado.=$this->nombres[$i];
				$resultado.='</a>';
				$resultado.='</li>';
			}



		$resultado.='</ul>';
		return $resultado;

	}

	function dibujaMenuExterno(){
		
		$resultado='';
		$resultado.='<ul class="nav '.$this->tipo.'">';
			for ($i=0; $i < count($this->nombres) ; $i++) { 
				
				$resultado.='<li class="">';
				$resultado.='<a href="http://'.$this->enlaces[$i].'" target="_blank">';

				$resultado.=$this->nombres[$i];
				$resultado.='</a>';
				$resultado.='</li>';
			}



		$resultado.='</ul>';
		return $resultado;

	}

}

 ?>