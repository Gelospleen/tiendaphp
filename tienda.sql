-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-05-2017 a las 13:44:59
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tienda`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `idCat` int(11) NOT NULL,
  `nombreCat` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `descripcionCat` longtext COLLATE utf8_spanish2_ci,
  `imagenCat` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`idCat`, `nombreCat`, `descripcionCat`, `imagenCat`) VALUES
(1, 'Pantalon corto', 'Ropa de verano y primavera a la ultima', 'pantalones.jpg'),
(2, 'Faldas ', 'Faldas mixtas para hombre y mujer', 'faldas.jpg'),
(3, 'Camiseta de manga corta', 'Una gran variedad de camisetas de manga corta', 'camisetas.jpg'),
(4, 'Botas camperas', 'Botas Camperas de moda', 'botas.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `enlaces`
--

CREATE TABLE `enlaces` (
  `idEn` int(11) NOT NULL,
  `descripcionEn` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `direccionEn` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `fechaEn` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `enlaces`
--

INSERT INTO `enlaces` (`idEn`, `descripcionEn`, `direccionEn`, `fechaEn`) VALUES
(1, 'Google: Gestor de búsquedas ', 'www.google.com', '2017-05-10'),
(2, 'Yahoo: Gestor de búsquedas', 'www.yahoo.es', '2017-05-09'),
(5, 'Pagina de compras de todo tipo', 'www.amazon.com', '2017-05-10'),
(9, 'Pagina de subastas', 'www.ebay.es', '2017-05-10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagenes`
--

CREATE TABLE `imagenes` (
  `idImg` int(11) NOT NULL,
  `descripcionImg` text COLLATE utf8_spanish2_ci,
  `archivoImg` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `idProd` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `imagenes`
--

INSERT INTO `imagenes` (`idImg`, `descripcionImg`, `archivoImg`, `idProd`) VALUES
(1, 'Vestido Primavera de Mujer', NULL, 0),
(2, 'Vestido Verano de Mujer', NULL, 0),
(3, 'Camiseta Primavera de Hombre', NULL, 0),
(4, 'Pantalón Verano de Hombre', NULL, 0),
(5, '1', '1.jpg', 49),
(6, '', '2.jpg', 49),
(8, '3', '3.jpg', 49);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE `pagos` (
  `idPago` int(11) NOT NULL,
  `nombrePago` varchar(25) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `logo` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `pagos`
--

INSERT INTO `pagos` (`idPago`, `nombrePago`, `logo`) VALUES
(1, 'cheque', NULL),
(2, 'contado', NULL),
(3, '30 días', NULL),
(4, '60 días', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `idPedido` int(11) NOT NULL,
  `fechaPedido` date NOT NULL,
  `cantidadPedido` int(11) NOT NULL,
  `idProd` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `idPago` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `idProd` int(11) NOT NULL,
  `nombreProd` varchar(25) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `descripcionProd` text COLLATE utf8_spanish2_ci,
  `precioProd` double(5,2) DEFAULT NULL,
  `unidadesProd` int(11) DEFAULT NULL,
  `fechaAlta` date DEFAULT NULL,
  `activado` bit(1) DEFAULT NULL,
  `idCat` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`idProd`, `nombreProd`, `descripcionProd`, `precioProd`, `unidadesProd`, `fechaAlta`, `activado`, `idCat`) VALUES
(1, 'Vestido', 'Vestido rojo', 12.95, 10, '2017-01-01', b'1', 2),
(2, 'Vestido', 'Vestido verde', 15.95, 12, '2017-01-02', b'1', 2),
(3, 'Camiseta', 'Camiseta blanca', 10.95, 8, '2017-02-10', b'1', 1),
(4, 'Pantalón', 'Pantalón negro', 19.95, 9, '2017-02-12', b'0', 1),
(5, 'pede blandit congue.', 'risus varius orci, in consequat enim diam vel arcu. Curabitur', 35.00, 8, '0000-00-00', b'1', 2),
(6, 'pellentesque, tellus sem', 'massa. Suspendisse eleifend. Cras sed leo. Cras vehicula aliquet libero.', 1.00, 2, '0000-00-00', b'1', 2),
(7, 'enim, sit amet', 'pharetra. Quisque ac libero nec ligula consectetuer rhoncus. Nullam velit', 7.00, 5, '0000-00-00', b'1', 1),
(8, 'dolor. Fusce feugiat.', 'a, auctor non, feugiat nec, diam. Duis mi enim, condimentum', 23.00, 3, '0000-00-00', b'0', 1),
(9, 'netus et malesuada', 'dictum. Phasellus in felis. Nulla tempor augue ac ipsum. Phasellus', 32.00, 8, '0000-00-00', b'0', 1),
(10, 'eu odio tristique', 'nonummy ut, molestie in, tempus eu, ligula. Aenean euismod mauris', 7.00, 5, '0000-00-00', b'1', 1),
(11, 'pede, nonummy ut,', 'eget laoreet posuere, enim nisl elementum purus, accumsan interdum libero', 26.00, 7, '2004-02-18', b'1', 2),
(12, 'lacinia orci, consectetue', 'risus a ultricies adipiscing, enim mi tempor lorem, eget mollis', 24.00, 1, '0000-00-00', b'0', 2),
(13, 'tortor nibh sit', 'dictum eu, eleifend nec, malesuada ut, sem. Nulla interdum. Curabitur', 47.00, 9, '0000-00-00', b'1', 1),
(14, 'ac ipsum. Phasellus', 'Phasellus at augue id ante dictum cursus. Nunc mauris elit,', 29.00, 1, '0000-00-00', b'0', 1),
(15, 'Nam tempor diam', 'pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque', 21.00, 3, '0000-00-00', b'0', 2),
(16, 'lectus quis massa.', 'egestas. Aliquam fringilla cursus purus. Nullam scelerisque neque sed sem', 12.00, 3, '2006-01-17', b'1', 1),
(17, 'dui nec urna', 'arcu. Morbi sit amet massa. Quisque porttitor eros nec tellus.', 7.00, 9, '0000-00-00', b'1', 2),
(18, 'vestibulum. Mauris magna.', 'arcu. Vestibulum ut eros non enim commodo hendrerit. Donec porttitor', 15.00, 4, '2008-09-17', b'0', 2),
(19, 'hymenaeos. Mauris ut', 'elit. Etiam laoreet, libero et tristique pellentesque, tellus sem mollis', 45.00, 4, '0000-00-00', b'1', 1),
(20, 'eleifend egestas. Sed', 'eu, eleifend nec, malesuada ut, sem. Nulla interdum. Curabitur dictum.', 29.00, 8, '0000-00-00', b'0', 2),
(21, 'sem ut dolor', 'orci luctus et ultrices posuere cubilia Curae; Phasellus ornare. Fusce', 35.00, 5, '0000-00-00', b'0', 2),
(22, 'Quisque porttitor eros', 'velit dui, semper et, lacinia vitae, sodales at, velit. Pellentesque', 45.00, 7, '2004-03-17', b'0', 2),
(23, 'velit dui, semper', 'enim, sit amet ornare lectus justo eu arcu. Morbi sit', 30.00, 3, '0000-00-00', b'0', 2),
(24, 'nec, euismod in,', 'Morbi neque tellus, imperdiet non, vestibulum nec, euismod in, dolor.', 13.00, 7, '0000-00-00', b'0', 2),
(25, 'vel quam dignissim', 'orci. Ut sagittis lobortis mauris. Suspendisse aliquet molestie tellus. Aenean', 41.00, 6, '2008-10-17', b'0', 2),
(26, 'vel turpis. Aliquam', 'erat nonummy ultricies ornare, elit elit fermentum risus, at fringilla', 24.00, 9, '0000-00-00', b'0', 2),
(27, 'cursus non, egestas', 'egestas ligula. Nullam feugiat placerat velit. Quisque varius. Nam porttitor', 21.00, 8, '2010-05-17', b'1', 1),
(28, 'vehicula et, rutrum', 'lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id,', 10.00, 10, '0000-00-00', b'1', 2),
(29, 'magnis dis parturient', 'sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum', 38.00, 4, '0000-00-00', b'1', 1),
(30, 'magnis dis parturient', 'rhoncus. Proin nisl sem, consequat nec, mollis vitae, posuere at,', 36.00, 5, '2004-01-18', b'0', 1),
(31, 'nunc id enim.', 'massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede', 5.00, 8, '2010-01-16', b'0', 1),
(32, 'amet ornare lectus', 'Nullam ut nisi a odio semper cursus. Integer mollis. Integer', 17.00, 5, '2002-09-17', b'1', 1),
(33, 'ad litora torquent', 'malesuada fames ac turpis egestas. Aliquam fringilla cursus purus. Nullam', 46.00, 4, '0000-00-00', b'1', 1),
(34, 'torquent per conubia', 'mauris id sapien. Cras dolor dolor, tempus non, lacinia at,', 1.00, 6, '2002-07-18', b'1', 1),
(35, 'Vestibulum ante ipsum', 'lorem ipsum sodales purus, in molestie tortor nibh sit amet', 34.00, 5, '0000-00-00', b'0', 1),
(36, 'amet, faucibus ut,', 'lectus quis massa. Mauris vestibulum, neque sed dictum eleifend, nunc', 12.00, 9, '0000-00-00', b'0', 2),
(37, 'arcu. Vestibulum ante', 'consequat auctor, nunc nulla vulputate dui, nec tempus mauris erat', 46.00, 1, '2008-07-16', b'1', 1),
(38, 'Nunc ac sem', 'ridiculus mus. Proin vel arcu eu odio tristique pharetra. Quisque', 17.00, 9, '2003-05-17', b'1', 2),
(39, 'dictum. Proin eget', 'ultricies adipiscing, enim mi tempor lorem, eget mollis lectus pede', 37.00, 3, '2007-04-17', b'1', 2),
(40, 'nec luctus felis', 'eget tincidunt dui augue eu tellus. Phasellus elit pede, malesuada', 33.00, 10, '2004-09-17', b'1', 1),
(41, 'a, magna. Lorem', 'dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer', 46.00, 3, '2011-07-17', b'0', 2),
(42, 'ac risus. Morbi', 'pede, nonummy ut, molestie in, tempus eu, ligula. Aenean euismod', 37.00, 7, '0000-00-00', b'1', 2),
(43, 'penatibus et magnis', 'dolor dapibus gravida. Aliquam tincidunt, nunc ac mattis ornare, lectus', 14.00, 4, '0000-00-00', b'0', 2),
(44, 'elit. Etiam laoreet,', 'ipsum porta elit, a feugiat tellus lorem eu metus. In', 12.00, 9, '2011-12-17', b'1', 1),
(45, 'Mauris ut quam', 'auctor ullamcorper, nisl arcu iaculis enim, sit amet ornare lectus', 22.00, 3, '0000-00-00', b'0', 1),
(46, 'tempus risus. Donec', 'lacus, varius et, euismod et, commodo at, libero. Morbi accumsan', 2.00, 8, '0000-00-00', b'0', 2),
(47, 'Donec at arcu.', 'Nullam nisl. Maecenas malesuada fringilla est. Mauris eu turpis. Nulla', 15.00, 3, '2006-01-16', b'0', 1),
(48, 'facilisis. Suspendisse co', 'penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin', 33.00, 10, '0000-00-00', b'1', 1),
(49, 'a felis ullamcorper', 'purus ac tellus. Suspendisse sed dolor. Fusce mi lorem, vehicula', 33.00, 4, '2012-04-17', b'0', 2),
(50, 'erat. Etiam vestibulum', 'feugiat non, lobortis quis, pede. Suspendisse dui. Fusce diam nunc,', 8.00, 9, '0000-00-00', b'0', 2),
(51, 'lacus vestibulum lorem,', 'vel turpis. Aliquam adipiscing lobortis risus. In mi pede, nonummy', 47.00, 7, '2007-04-17', b'0', 2),
(52, 'est, vitae sodales', 'ornare, libero at auctor ullamcorper, nisl arcu iaculis enim, sit', 30.00, 5, '2005-11-16', b'1', 2),
(53, 'Quisque fringilla euismod', 'adipiscing fringilla, porttitor vulputate, posuere vulputate, lacus. Cras interdum. Nunc', 13.00, 1, '0000-00-00', b'0', 2),
(54, 'tempus, lorem fringilla', 'libero at auctor ullamcorper, nisl arcu iaculis enim, sit amet', 43.00, 2, '2007-09-17', b'0', 2),
(55, 'ullamcorper, nisl arcu', 'consectetuer, cursus et, magna. Praesent interdum ligula eu enim. Etiam', 34.00, 4, '0000-00-00', b'1', 2),
(56, 'lacus pede sagittis', 'ut eros non enim commodo hendrerit. Donec porttitor tellus non', 41.00, 9, '2001-09-18', b'1', 1),
(57, 'Proin ultrices. Duis', 'justo. Proin non massa non ante bibendum ullamcorper. Duis cursus,', 7.00, 3, '2012-11-17', b'0', 2),
(58, 'justo faucibus lectus,', 'nascetur ridiculus mus. Aenean eget magna. Suspendisse tristique neque venenatis', 28.00, 2, '0000-00-00', b'0', 1),
(59, 'ac tellus. Suspendisse', 'lectus. Nullam suscipit, est ac facilisis facilisis, magna tellus faucibus', 16.00, 2, '2003-05-17', b'1', 2),
(60, 'dui, nec tempus', 'elit, dictum eu, eleifend nec, malesuada ut, sem. Nulla interdum.', 34.00, 3, '0000-00-00', b'0', 1),
(61, 'Aliquam adipiscing lobort', 'justo faucibus lectus, a sollicitudin orci sem eget massa. Suspendisse', 25.00, 1, '0000-00-00', b'0', 1),
(62, 'egestas. Sed pharetra,', 'libero. Integer in magna. Phasellus dolor elit, pellentesque a, facilisis', 43.00, 7, '0000-00-00', b'1', 2),
(63, 'Nulla facilisi. Sed', 'id risus quis diam luctus lobortis. Class aptent taciti sociosqu', 31.00, 1, '0000-00-00', b'1', 2),
(64, 'turpis egestas. Fusce', 'quis lectus. Nullam suscipit, est ac facilisis facilisis, magna tellus', 13.00, 4, '2010-12-17', b'0', 1),
(65, 'Sed auctor odio', 'gravida mauris ut mi. Duis risus odio, auctor vitae, aliquet', 14.00, 2, '2003-03-18', b'0', 2),
(66, 'quis accumsan convallis,', 'adipiscing. Mauris molestie pharetra nibh. Aliquam ornare, libero at auctor', 10.00, 9, '0000-00-00', b'0', 1),
(67, 'tellus faucibus leo,', 'lacinia at, iaculis quis, pede. Praesent eu dui. Cum sociis', 4.00, 4, '0000-00-00', b'1', 2),
(68, 'tempor diam dictum', 'cursus vestibulum. Mauris magna. Duis dignissim tempor arcu. Vestibulum ut', 1.00, 9, '2008-01-17', b'1', 1),
(69, 'scelerisque, lorem ipsum', 'natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 44.00, 3, '0000-00-00', b'0', 2),
(70, 'viverra. Donec tempus,', 'nec quam. Curabitur vel lectus. Cum sociis natoque penatibus et', 11.00, 4, '0000-00-00', b'0', 2),
(71, 'nec, mollis vitae,', 'sem elit, pharetra ut, pharetra sed, hendrerit a, arcu. Sed', 36.00, 1, '0000-00-00', b'1', 1),
(72, 'molestie dapibus ligula.', 'Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet', 39.00, 4, '0000-00-00', b'0', 2),
(73, 'dis parturient montes,', 'Nam consequat dolor vitae dolor. Donec fringilla. Donec feugiat metus', 15.00, 5, '2007-12-16', b'0', 1),
(74, 'nisi. Aenean eget', 'vel, vulputate eu, odio. Phasellus at augue id ante dictum', 25.00, 2, '2006-07-17', b'0', 2),
(75, 'tellus. Nunc lectus', 'mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus', 30.00, 4, '2005-01-18', b'1', 2),
(76, 'natoque penatibus et', 'pellentesque massa lobortis ultrices. Vivamus rhoncus. Donec est. Nunc ullamcorper,', 17.00, 8, '0000-00-00', b'0', 2),
(77, 'non nisi. Aenean', 'Nunc mauris sapien, cursus in, hendrerit consectetuer, cursus et, magna.', 2.00, 7, '2009-08-17', b'1', 1),
(78, 'Suspendisse eleifend. Cra', 'iaculis odio. Nam interdum enim non nisi. Aenean eget metus.', 38.00, 4, '2012-08-16', b'0', 1),
(79, 'quis, pede. Suspendisse', 'mus. Proin vel nisl. Quisque fringilla euismod enim. Etiam gravida', 49.00, 1, '0000-00-00', b'1', 2),
(80, 'lectus quis massa.', 'mauris ut mi. Duis risus odio, auctor vitae, aliquet nec,', 26.00, 8, '2006-04-17', b'0', 1),
(81, 'et malesuada fames', 'Proin sed turpis nec mauris blandit mattis. Cras eget nisi', 3.00, 2, '0000-00-00', b'0', 2),
(82, 'Nullam nisl. Maecenas', 'lobortis, nisi nibh lacinia orci, consectetuer euismod est arcu ac', 42.00, 3, '2007-10-17', b'0', 2),
(83, 'ligula. Nullam feugiat', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur', 14.00, 4, '2004-09-18', b'1', 1),
(84, 'vitae, orci. Phasellus', 'dolor. Nulla semper tellus id nunc interdum feugiat. Sed nec', 18.00, 4, '2008-06-17', b'0', 2),
(85, 'Aliquam rutrum lorem', 'dictum sapien. Aenean massa. Integer vitae nibh. Donec est mauris,', 14.00, 8, '2012-05-16', b'0', 2),
(86, 'odio semper cursus.', 'mauris id sapien. Cras dolor dolor, tempus non, lacinia at,', 35.00, 10, '2003-10-17', b'1', 1),
(87, 'sed, facilisis vitae,', 'elit, pharetra ut, pharetra sed, hendrerit a, arcu. Sed et', 14.00, 6, '0000-00-00', b'1', 2),
(88, 'vitae diam. Proin', 'In faucibus. Morbi vehicula. Pellentesque tincidunt tempus risus. Donec egestas.', 47.00, 3, '0000-00-00', b'1', 2),
(89, 'In lorem. Donec', 'tellus eu augue porttitor interdum. Sed auctor odio a purus.', 32.00, 1, '2005-03-17', b'0', 1),
(90, 'et tristique pellentesque', 'ac mattis ornare, lectus ante dictum mi, ac mattis velit', 2.00, 3, '2002-10-17', b'0', 2),
(91, 'pede. Suspendisse dui.', 'aliquam eros turpis non enim. Mauris quis turpis vitae purus', 18.00, 7, '0000-00-00', b'1', 2),
(92, 'mollis lectus pede', 'erat volutpat. Nulla facilisis. Suspendisse commodo tincidunt nibh. Phasellus nulla.', 12.00, 10, '0000-00-00', b'0', 1),
(93, 'nisi nibh lacinia', 'turpis. In condimentum. Donec at arcu. Vestibulum ante ipsum primis', 31.00, 7, '0000-00-00', b'1', 2),
(94, 'leo. Vivamus nibh', 'metus. In lorem. Donec elementum, lorem ut aliquam iaculis, lacus', 12.00, 1, '0000-00-00', b'1', 1),
(95, 'massa. Quisque porttitor', 'et malesuada fames ac turpis egestas. Fusce aliquet magna a', 22.00, 7, '0000-00-00', b'1', 1),
(96, 'mollis non, cursus', 'eu erat semper rutrum. Fusce dolor quam, elementum at, egestas', 25.00, 8, '0000-00-00', b'1', 1),
(97, 'Integer urna. Vivamus', 'a mi fringilla mi lacinia mattis. Integer eu lacus. Quisque', 35.00, 2, '0000-00-00', b'0', 2),
(98, 'ultrices iaculis odio.', 'ornare, facilisis eget, ipsum. Donec sollicitudin adipiscing ligula. Aenean gravida', 5.00, 6, '0000-00-00', b'0', 1),
(99, 'commodo tincidunt nibh.', 'Nullam scelerisque neque sed sem egestas blandit. Nam nulla magna,', 50.00, 7, '0000-00-00', b'0', 2),
(100, 'ut, sem. Nulla', 'magna nec quam. Curabitur vel lectus. Cum sociis natoque penatibus', 16.00, 10, '2007-10-17', b'1', 1),
(101, 'eu dolor egestas', 'ullamcorper viverra. Maecenas iaculis aliquet diam. Sed diam lorem, auctor', 21.00, 9, '0000-00-00', b'0', 1),
(102, 'varius et, euismod', 'ut eros non enim commodo hendrerit. Donec porttitor tellus non', 7.00, 7, '0000-00-00', b'1', 2),
(103, 'lectus sit amet', 'Nullam enim. Sed nulla ante, iaculis nec, eleifend non, dapibus', 17.00, 4, '0000-00-00', b'1', 2),
(105, 'camiseta azul aquamarine', 'Camiseta molona', 45.50, 9, '2017-05-05', b'1', 1),
(106, 'Pantalon de lentejuelas', 'Pantalos de lentejuelas con bluetooth', 120.00, 3, '2017-05-05', b'1', 1),
(107, 'Pantalon de cuadros', 'Pantalon de cuadros de toda la vida', 25.00, 0, '2017-05-05', b'1', 1),
(108, 'Camiseta amarilla de los ', 'Camiseta de los Simpsons de moda', 15.00, 7, '2017-05-08', b'1', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idUsuario` int(11) NOT NULL,
  `nombre` varchar(25) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `apellidos` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `login` varchar(25) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `password` varchar(15) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `correo` varchar(25) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `avatar` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`idCat`);

--
-- Indices de la tabla `enlaces`
--
ALTER TABLE `enlaces`
  ADD PRIMARY KEY (`idEn`);

--
-- Indices de la tabla `imagenes`
--
ALTER TABLE `imagenes`
  ADD PRIMARY KEY (`idImg`);

--
-- Indices de la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD PRIMARY KEY (`idPago`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`idPedido`),
  ADD KEY `fkpedidosproductos` (`idProd`),
  ADD KEY `fkpedidosusuarios` (`idUsuario`),
  ADD KEY `fkpedidospagos` (`idPago`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`idProd`),
  ADD KEY `fkproductoscategorias` (`idCat`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idUsuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `idCat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `enlaces`
--
ALTER TABLE `enlaces`
  MODIFY `idEn` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `imagenes`
--
ALTER TABLE `imagenes`
  MODIFY `idImg` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `pagos`
--
ALTER TABLE `pagos`
  MODIFY `idPago` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `idPedido` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `idProd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD CONSTRAINT `fkpedidospagos` FOREIGN KEY (`idPago`) REFERENCES `pagos` (`idPago`),
  ADD CONSTRAINT `fkpedidosproductos` FOREIGN KEY (`idProd`) REFERENCES `productos` (`idProd`),
  ADD CONSTRAINT `fkpedidosusuarios` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`idUsuario`);

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `fkproductoscategorias` FOREIGN KEY (`idCat`) REFERENCES `categorias` (`idCat`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
