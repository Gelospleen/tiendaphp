<br>
<h4>Listado de productos por Categoria</h4>

<?php 
if(isset($_GET['idCat'])){
	$idCat=$_GET['idCat'];

	if(is_numeric($idCat)){


		$sql="SELECT * FROM categorias WHERE idCat=$idCat";
		$consulta=mysqli_query($conexion, $sql);
		if(mysqli_num_rows($consulta)==1){

			$r=mysqli_fetch_array($consulta);
			 ?>


			 	<h4><?php echo $r['nombreCat']; ?>
			 
			 <small><?php echo $r['descripcionCat']; ?></small>
			 <img src="imagenes/categorias/<?php echo $r['imagenCat']; ?>" width="100" class="img-responsive">
				</h4>
			 <hr>
			<section class="row">	
			 <?php 
			$sql="SELECT * FROM productos WHERE idCat=$idCat";
			$consulta=mysqli_query($conexion, $sql);
				if(mysqli_num_rows($consulta)>0){

				while($r=mysqli_fetch_array($consulta)){
					?>
					<article class="col-md-3 col-sm-6" style="min-height:200px;">
						<header>
							<h3>
								<?php echo $r['nombreProd'];?>
							</h3>
							
						</header>	
						<section>			
							<?php echo $r['descripcionProd'];?>
						</section>
						
					</article>
				<?php 


				}
				?>
				</section>
				<?php 
				}else{
					echo 'Lo siento... no hay productos en esta categoria';
				}
		}else{
				echo 'Hay un problema con las categorias';
		}  
	}else{
		echo 'El codigo de idCat ha de ser numerico';
	  
	}
}else{
	echo 'Hay que indicar un idCat';
}
?>