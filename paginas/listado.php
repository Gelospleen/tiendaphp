
<ul class="nav nav-tabs">
	<li><a href="index.php?p=listado.php&campo=nombreProd&orden=ASC">Nombre A</a></li>
	<li><a href="index.php?p=listado.php&campo=nombreProd&orden=DESC">Nombre D</a></li>
	<li><a href="index.php?p=listado.php&campo=precioProd&orden=ASC">Precio A</a></li>
	<li><a href="index.php?p=listado.php&campo=precioProd&orden=DESC">Precio D</a></li>
	<li><a href="index.php?p=listado.php&campo=unidadesProd&orden=ASC">Unidades A</a></li>
	<li><a href="index.php?p=listado.php&campo=unidadesProd&orden=DESC">Unidades D</a></li>
	<li><a href="index.php?p=listado.php&campo=fechaAlta&orden=ASC">Fecha A</a></li>
	<li><a href="index.php?p=listado.php&campo=fechaAlta&orden=DESC">Fecha D</a></li>

</ul>


<?php 

	if(isset($_GET['campo'])){

	$campo=$_GET['campo'];
}else{ 

	$campo='nombreProd'; 

}

if(isset($_GET['orden'])){

	$orden=$_GET['orden'];
}else{ 

	$orden='ASC'; 
}
?>

<?php 


	

switch ($campo) {
	case 'nombreProd':
		$nombreMostrar='Nombre de producto';
		break;
	case 'precioProd':
		$nombreMostrar='Precio de producto';
		break;
	case 'unidadesProd':
		$nombreMostrar='Unidades de producto en stock';
		break;
	case 'fechaAlta':
		$nombreMostrar='Fecha de alta de producto';
		break;	
	
}

 switch ($orden) {
 	case 'ASC':
 	$ordenMostrar='Ascendente';
 	break;
 	case 'DESC':
 	$ordenMostrar='Descendente';
 	break;
 }
?>

<h4>Ordenado por <strong><?php echo $nombreMostrar ?></strong> de forma <strong><?php echo $ordenMostrar; ?></strong></h4>

<section class="row">
<?php 


//  Pregunta

	// $sql="SELECT * FROM productos ORDER BY $campo $orden LIMIT 0,10";
	// $sql="SELECT * FROM productos ORDER BY idProd DESC";
	$sql="SELECT * FROM productos INNER JOIN categorias ON productos.idCat=categorias.idCat ORDER BY $campo $orden" ;

//  Ejecutar la consulta

	$consulta=mysqli_query($conexion, $sql);

// 	Procesar los resultados de ejecutar la consulta

	while ($r=mysqli_fetch_array($consulta)) {
?>

	<article class="col-md-3 col-sm-6" style="min-height:400px; border: 1px solid black;">
		<header>
			<h3>
				<?php echo $r['nombreProd'];?>
				<small><?php echo $r['precioProd'];?> Euros</small> 
				<small><?php echo $r['fechaAlta']; ?></small>
				<span class="label label-success">
					<?php echo $r['unidadesProd']; ?> Unds
				</span>
				- <a href="index.php?p=detalle.php&id=<?php echo $r['idProd']; ?>">Ver Producto</a>
				- <a href="index.php?p=borrar.php&id=<?php echo $r['idProd']; ?>">Borrar Producto</a>
				- <a href="index.php?p=modificar.php&id=<?php echo $r['idProd']; ?>">Modificar Producto</a>

			</h3>
			<h4>Categoria: <?php echo $r['nombreCat']; ?></h4>
		</header>	
		<section>			
			<?php echo $r['descripcionProd'];?>
		</section>
		
	</article>

<?php 
	}
?>
</section>
