<?php 
// Fichero: paginas/detalle.php
// Funcion: Recibira el id del producto, y mostrara la info de dicho
// 			producto

//  Tengo que recoger el id de producto que me han mandado
$id=$_GET['id'];

// Confecciono la pregunta a mi base de datos
$sql="SELECT * FROM productos WHERE idProd=$id";

//  Ejecuto la consulta
$consulta=mysqli_query($conexion, $sql);

// Extraigo el unico resultado
$r=mysqli_fetch_array($consulta);

$producto=new Producto($r);

?>

<article>
	<header>
		<h2><?php echo $producto->nombreProd; ?></h2>
	</header>
	<section>
		<p><?php echo $producto->descripcionProd; ?></p>
	</section>
	<footer>
		<h4><?php echo $producto->precioProd; ?> Euros</h4>
		<small>
			Iva: <?php echo $producto->dimeIva(); ?>
		</small>
		<h4><?php echo $producto->unidadesProd; ?> Unidades en stock</h4>
	</footer>
	<a href="index.php?p=listado.php"></a>
</article>
<hr>


<form role="form" method="post" action="index.php?p=detalle.php&id=<?php echo $id; ?>" enctype="multipart/form-data">

  <div class="form-group">
    <label for="descripcionImg">Nombre de la imagen</label>
    <input type="text" class="form-control" id="descripcionImg" name="descripcionImg" placeholder="Introduce el nombre de la imagen">
  </div>

     <div class="form-group">
    <label for="archivoImg">Archivo de imagen</label>
    <input type="file" class="form-control" id="archivoImg" name="archivoImg">
  </div> 

  <input type="hidden" name="idProd" value="<?php echo $id; ?>">


  <div class="form-group">
    <input type="submit" class="form-control" name="insertar" value="Alta de imagen">
  </div>

 </form>

 <?php 
  // Vamos a procesar la informacion que enviamos desde el formulario 
 if(isset($_POST['insertar'])){
 		$descripcionImg=$_POST['descripcionImg'];
 		$idProd=$_POST['idProd'];

 		// $FILES['archivoImg']['name'];
 		// $FILES['archivoImg']['tmp_name'];
 		// $FILES['archivoImg']['size'];
 		// $FILES['archivoImg']['type'];
 		// $FILES['archivoImg']['error'];

 		$nombreImagen=$_FILES['archivoImg']['name'];

 		move_uploaded_file($_FILES['archivoImg']['tmp_name'], 'imagenes/'.$nombreImagen);

 		$sqlImg="INSERT INTO imagenes(descripcionImg, archivoImg, idProd)VALUES('$descripcionImg','$nombreImagen', $idProd)";
 		$consultaImg=mysqli_query($conexion, $sqlImg);

 }

 ?>

<hr>
<?php 
	// $sql="SELECT * FROM imagenes WHERE idProd=$id";
	// $consulta=mysqli_query($conexion, $sql);
	// while($r=mysqli_fetch_array($consulta)){
	for($i=0; $i< count($producto->imagenes); $i++){

?>
	<figure style="display:inline-block; vertical-align: top;">
	<img src="imagenes/<?php echo $producto->imagenes[$i];?>" width="200">
<!-- 	<figcaption>
			<?php echo $r['descripcionImg']; ?>
	</figcaption> -->

	</figure>
	<?php 

	}

 ?>



